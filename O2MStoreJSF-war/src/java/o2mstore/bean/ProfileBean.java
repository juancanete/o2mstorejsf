/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package o2mstore.bean;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;
import o2mstore.ejb.ProductoFacade;
import o2mstore.ejb.UsuarioFacade;
import o2mstore.entity.Producto;
import o2mstore.entity.Usuario;
import sun.misc.BASE64Encoder;

/**
 *
 * @author David
 */
@ManagedBean
@SessionScoped
public class ProfileBean implements Serializable{

    
    //Bean variables
    private String email;
    private String password;
    private String password2;
    private String name;
    private String lastName;
    private String rol;
    private Part imagen; 
   
    
    
    @EJB
    private ProductoFacade productoFacade;
    @EJB
    private UsuarioFacade usuarioFacade;
    
     private List<Producto> listProduct;
     private Producto prod;
    
    
    public ProfileBean() {
   
    }
    
    @PostConstruct
    public void encontrar(){
        FacesContext context = FacesContext.getCurrentInstance();
	LoginBean sesion = (LoginBean)context.getApplication().evaluateExpressionGet(context, "#{loginBean}", LoginBean.class);
        listProduct = productoFacade.findByPropietary(sesion.user.getIdusuario());
        System.out.println("lista de productos por propietario"+listProduct);
        
    }
    
    
    public String editProfile(Usuario user) throws IOException{
        
        String email = this.getEmail();
        String nombre = this.getName();
        String apellidos = this.getLastName();
        String password = this.getPassword();
        String rol = this.getRol();
        Part imagen = this.imagen;
       

        user.setNombre(nombre);
        user.setApellidos(apellidos);
        user.setContrasena(password);
        user.setEmail(email);
        user.setFechanac(new Date(2011, 12, 20));
        if(user.getRol().equals("admin"))
            user.setRol("admin");
        else
            user.setRol("user");
        
        InputStream inputStream = imagen.getInputStream();          
        FileOutputStream outputStream = new FileOutputStream(getFilename(imagen));  
        long longitudimagen= imagen.getSize();
        
        byte[] buffer = new byte[(int) longitudimagen];          
        int bytesRead = 0;  
        while(true) {                          
            bytesRead = inputStream.read(buffer);  
            if(bytesRead > 0) {  
                outputStream.write(buffer, 0, bytesRead);  
            }else {  
                break;  
            }                         
        }
        
        outputStream.close();  
        inputStream.close();  
        
        BASE64Encoder e = new BASE64Encoder();
        String datos = e.encodeBuffer(buffer);
        user.setImagen(datos);
                

       System.out.println(imagen);
        usuarioFacade.edit(user);


        //HttpSession miSesion= request.getSession(true);
        //miSesion.setAttribute("user", user);
                
        //response.sendRedirect("indexStore.jsp");
        return "indexStore.xhtml";
        
    }
    
//    public String upload() throws IOException {  
//        try (InputStream inputStream = getImagen().getInputStream(); FileOutputStream outputStream = new FileOutputStream(getFilename(getImagen()))) {
//            
//            byte[] buffer = new byte[4096];
//            int bytesRead = 0;
//            while(true) {
//                bytesRead = inputStream.read(buffer);
//                if(bytesRead > 0) {
//                    outputStream.write(buffer, 0, bytesRead);
//                }else {
//                    break;
//                }
//            }
//        }  
//         
//        return "success";  
//    }  
//  
    private static String getFilename(Part part) {  
        for (String cd : part.getHeader("content-disposition").split(";")) {  
            if (cd.trim().startsWith("filename")) {  
                String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");  
                return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.  
            }  
        }  
        return null;  
    }  
    
    public void borrarProducto(Producto prod){
        
       
        FacesContext context = FacesContext.getCurrentInstance();
        LoginBean sesion = (LoginBean)context.getApplication().evaluateExpressionGet(context, "#{loginBean}", LoginBean.class);
        
        Producto product = productoFacade.find(new Integer(prod.getIdproducto()));
        productoFacade.remove(product);
        listProduct = productoFacade.findByPropietary(sesion.user.getIdusuario());
        
    }
    
    
    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

        /**
     * @return the rol
     */
    public String getRol() {
        return rol;
    }

    /**
     * @param rol the rol to set
     */
    public void setRol(String rol) {
        this.rol = rol;
    }

    /**
     * @return the password2
     */
    public String getPassword2() {
        return password2;
    }

    /**
     * @param password2 the password2 to set
     */
    public void setPassword2(String password2) {
        this.password2 = password2;
    }
    
   
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the listProduct
     */
    public List<Producto> getListProduct() {
        return listProduct;
    }

    /**
     * @param listProduct the listProduct to set
     */
    public void setListProduct(List<Producto> listProduct) {
        this.listProduct = listProduct;
    }

    /**
     * @return the prod
     */
    public Producto getProd() {
        return prod;
    }

    /**
     * @param prod the prod to set
     */
    public void setProd(Producto prod) {
        this.prod = prod;
    }

//    /**
//     * @return the sesion
//     */
//    public LoginBean getSesion() {
//        return sesion;
//    }
//
//    /**
//     * @param sesion the sesion to set
//     */
//    public void setSesion(LoginBean sesion) {
//        this.sesion = sesion;
//    }

    /**
     * @return the imagen
     */
    public Part getImagen() {
        return imagen;
    }

    /**
     * @param imagen the imagen to set
     */
    public void setImagen(Part imagen) {
        this.imagen = imagen;
    }

   

    
}
