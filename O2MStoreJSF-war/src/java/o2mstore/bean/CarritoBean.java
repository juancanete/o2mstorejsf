/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package o2mstore.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;
import o2mstore.ejb.PedidoFacade;
import o2mstore.ejb.ProductoFacade;
import o2mstore.ejb.RealizaFacade;
import o2mstore.entity.Pedido;
import o2mstore.entity.Producto;
import o2mstore.entity.Realiza;
import o2mstore.entity.Usuario;

/**
 *
 * @author Juan
 */
@ManagedBean
@SessionScoped
public class CarritoBean implements Serializable {
    
    
    private static Map<Integer,List<Producto>> mapCurrentReq;
    
    @EJB
    private RealizaFacade realizaFacade;
    @EJB
    private ProductoFacade productoFacade;
    @EJB
    private PedidoFacade pedidoFacade;
    
    //protected String category;
    
    protected List<Producto> listProduct;
    
    protected Producto prod;
    protected Producto prodDel;
    
    protected LoginBean loginBean;
    private String busquedaProd;
    
    //Booleans variables
    protected boolean initPage;
    protected boolean endOrder;
    
    public CarritoBean()
    {
        
    }
    
    
    public boolean isInitPage() {
        return initPage;
    }

    public void setInitPage(boolean initPage) {
        System.out.println("booleanInitPage: " +initPage);
        this.initPage = initPage;
    }
    
    public boolean isEndOrder() {
        return endOrder;
    }

    public void setEndOrder(boolean endOrder) {
        this.endOrder = endOrder;
    }
    
    public List<Producto> getListProduct() {
        return listProduct;
    }

    public void setListProduct(List<Producto> listProduct) {
        this.listProduct = listProduct;
    }
    
    public static Map<Integer, List<Producto>> getMapCurrentReq() {
        return mapCurrentReq;
    }

    public Producto getProd() {
        return prod;
    }

    public void setProd(Producto prod) {
        this.prod = prod;
    }

    public Producto getProdDel() {
        return prodDel;
    }

    public void setProdDel(Producto prodDel) {
        this.prodDel = prodDel;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * @return the busquedaProd
     */
    public String getBusquedaProd() {
        return busquedaProd;
    }

    /**
     * @param busquedaProd the busquedaProd to set
     */
    public void setBusquedaProd(String busquedaProd) {
        this.busquedaProd = busquedaProd;
    }
    
    public void buyOneProduct(Producto product)
    {
        System.out.println("Comprar producto: " + product);
        this.insertMapCurrentReq(product);
    }
    
    public void insertMapCurrentReq(Producto product) {
        
        if(mapCurrentReq == null)
        {
            mapCurrentReq = new HashMap<Integer, List<Producto>>();
        }
        
        List <Producto> listProd = mapCurrentReq.get(loginBean.user.getIdusuario());
        
        if(listProd == null)
        {
            listProd = new LinkedList<>();
        }
        
        listProd.add(product);
        mapCurrentReq.put(loginBean.user.getIdusuario(), listProd);
    }
    
    public List<Producto> productsFromMap(Usuario user)
    {
        if(mapCurrentReq != null)
        {
            return mapCurrentReq.get(user.getIdusuario());
        }else
        {
            List<Producto> list = new LinkedList<Producto>();
            
            return list;
        }
    }
    
    public void updateListProductByCat(String category)
    {
        listProduct = productoFacade.findByCategory(category);
        this.setInitPage(false);
        System.out.println("Categories: " + listProduct.toString());
        
        //return "categoriesProd";
    }
    
    public void deleteProduct(Producto product)
    {
        List<Producto> listProd;
        System.out.println("deleteProduct: " + loginBean.user + "prod: " + product);
        
        if(mapCurrentReq != null)
        {
            listProd = mapCurrentReq.get(loginBean.user.getIdusuario());
            listProd.remove(product);
        }
        
        //return "indexStore";
    }
    
    public String deleteAllProducts ()
    {
        Pedido pedido = new Pedido();
        pedido.setIdpedido(pedidoFacade.getMaxID()+1);
        pedido.setFecha(new Date());
        
        pedidoFacade.create(pedido);
         
        //Set new value EndOrder
        this.setEndOrder(true);
        this.setInitPage(true);
        System.out.println("DELETEALLPRODUCTS!! " + this.isEndOrder());
       
        Usuario user = loginBean.user;
         
        // Integer idpedido = request.getParameter(pedidoFacade.idpedido);
        List<Producto> list2 = this.productsFromMap(user);
        
        Realiza item = new Realiza();
        
        for(Producto p: list2)
        {
            item.setIdrealiza(realizaFacade.findMaxIdRealiza()+1);
            item.setIdusuario(user);
            item.setIdproducto(p);
            item.setIdpedido(pedido);
            
            realizaFacade.create(item);
        }
        
        //Delete user's products
        mapCurrentReq.remove(user.getIdusuario());
        
        return "indexStore";
    }
    
    public void busquedaProducto(){
         System.out.println("busqueda: " + this.getBusquedaProd());
        this.setInitPage(false);
        
        setListProduct(productoFacade.findByNameAll(this.getBusquedaProd()));
        
        System.out.println("size: " + listProduct.size());
        for(Producto p : listProduct){
            System.out.println(p);
        }
    }
    
    @PostConstruct
    public void leerPedidos()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        loginBean = (LoginBean) context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), "#{loginBean}", LoginBean.class)
                .getValue(context.getELContext());
        
        mapCurrentReq = new HashMap<Integer, List<Producto>>();
        this.setInitPage(true);
        this.setEndOrder(false);
        listProduct = productoFacade.findAll();
        
    }
}
