/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package o2mstore.bean;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.Part;
import o2mstore.ejb.ProductoFacade;
import o2mstore.entity.Producto;
import sun.misc.BASE64Encoder;

/**
 *
 * @author VictorCg
 */
@ManagedBean
@SessionScoped
public class EditProductBean implements Serializable {

    @EJB
    private ProductoFacade productoFacade;

    //Bean parameters
    protected String nombre;
    protected String categoria;
    protected String descripcion;
    protected Part imagen;

    private boolean editProd;

    protected Integer productIdToEdit;

    //Constructor
    public EditProductBean() {
        
    }
    
    //Getters and Setters
    public ProductoFacade getProductoFacade() {
        return productoFacade;
    }

    public void setProductoFacade(ProductoFacade productoFacade) {
        this.productoFacade = productoFacade;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public Part getImagen() {
        return imagen;
    }

    public void setImagen(Part imagen) {
        this.imagen = imagen;
    }

    public boolean isEditProd() {
        return editProd;
    }

    public void setEditProd(boolean editProd) {
        this.editProd = editProd;
    }

    public Integer getProductIdToEdit() {
        return productIdToEdit;
    }

    public void setProductIdToEdit(Integer productIdToEdit) {
        this.productIdToEdit = productIdToEdit;
    }
    
    //Own Methods
    public void prepareEditProduct(Producto productoAnt) {
        Producto producto = productoFacade.findById(productoAnt.getIdproducto());
        productIdToEdit = producto.getIdproducto();
        this.setEditProd(!this.editProd);
    }

    public void editProduct() throws IOException {
        
        Producto prod = productoFacade.findById(productIdToEdit);

        if (prod == null) {
            this.setEditProd(false);
        } 
        else 
        {
            prod.setNombrep(nombre);
            
            if(descripcion != null)
                prod.setDescripcion(descripcion);

            switch (categoria) {
                case "android":
                case "blackbery":
                case "iphone": {
                    prod.setCategoria("aplicaciones");
                    prod.setSubcategoria(categoria);
                    break;
                }
                case "terror":
                case "comedia":
                case "accion":
                case "cienciaficcion":
                case "animacion":
                case "drama":
                case "thriller": {
                    prod.setCategoria("peliculas");
                    prod.setSubcategoria(categoria);
                    break;
                }
                case "pop":
                case "rock":
                case "metal":
                case "clasica":
                case "hiphop":
                case "disco": {
                    prod.setCategoria("musica");
                    prod.setSubcategoria(categoria);
                    break;
                }
                case "ciencias":
                case "cocina":
                case "historia":
                case "informatica":
                case "clasicos":
                case "infantil": {
                    prod.setCategoria("libros");
                    prod.setSubcategoria(categoria);
                    break;
                }
                case "aplicaciones": {
                    prod.setCategoria(categoria);
                    prod.setSubcategoria("Sin Especificar");
                    break;
                }
                case "pelicula": {
                    prod.setCategoria(categoria);
                    prod.setSubcategoria("Sin Especificar");
                    break;
                }
                case "musica": {
                    prod.setCategoria(categoria);
                    prod.setSubcategoria("Sin Especificar");
                    break;
                }
                case "libros": {
                    prod.setCategoria(categoria);
                    prod.setSubcategoria("Sin Especificar");
                    break;
                }
            }
            
            if(imagen != null)
            {
                InputStream inputStream = imagen.getInputStream();
                FileOutputStream outputStream = new FileOutputStream(getFilename(imagen));
                long longitudimagen = imagen.getSize();

                byte[] buffer = new byte[(int) longitudimagen];
                int bytesRead = 0;
                while (true) {
                    bytesRead = inputStream.read(buffer);
                    if (bytesRead > 0) {
                        outputStream.write(buffer, 0, bytesRead);
                    } else {
                        break;
                    }
                }

                outputStream.close();
                inputStream.close();

                BASE64Encoder e = new BASE64Encoder();
                String datos = e.encodeBuffer(buffer);

                prod.setImagen(datos);
            }
            
            productoFacade.edit(prod);

            this.setEditProd(false);
        }
    }

    private static String getFilename(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.  
            }
        }
        return null;
    }

    //PostConstruct
    @PostConstruct
    public void encontrar() {
        this.setEditProd(false);
    }
}
