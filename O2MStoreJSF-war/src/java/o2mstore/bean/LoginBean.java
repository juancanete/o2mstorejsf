/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package o2mstore.bean;

import java.io.Serializable;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import o2mstore.ejb.ProductoFacade;
import o2mstore.ejb.UsuarioFacade;
import o2mstore.entity.Usuario;

/**
 *
 * @author masterinftel22
 */
@ManagedBean
@SessionScoped
public class LoginBean implements Serializable {
    
    //Bean variables
    protected String email;
    protected String password;
    protected String password2;
    protected String name;
    protected String lastName;
    protected String terms;
    protected Date date;
    protected Usuario user;
    
    protected boolean login;
    
    protected boolean showLogin;
    
    @EJB
    private ProductoFacade productoFacade;
    
    @EJB
    private UsuarioFacade usuarioFacade;

    
    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }
    
    public String createSession ()
    {
        String password = this.password;
        String email = this.email;
        
        setUser(usuarioFacade.findByEmail(email));
        
        
        if(this.getUser() != null){
            /*if(usuarioFacade.tryLogin(email, password)){

                miSesion.setAttribute("user", user);
                //response.sendRedirect("indexStore.jsp");
                //return "indexStore";
            }
            else{
                
                request.setAttribute("login", "Contraseña erronea!!!");
                RequestDispatcher rd;

                //rd = request.getRequestDispatcher("indexStore.jsp");
                //rd.forward(request, response);
                
                
                
            }
        }
        else{
            
            request.setAttribute("login", "El e-mail no esta registrado!!!");
            RequestDispatcher rd;

            //rd = request.getRequestDispatcher("indexStore.jsp");
            //rd.forward(request, response);
        }*/
            System.out.println("LOGINNNNN!!!!");
            setLogin(true);
            setShowLogin(false);
            
            return "indexStore";
        }
        
        System.out.println("unLOGINNNNN!!!!");
        return "";
    }
    
    public String doLogOut ()
    {
        System.out.println("NO HAY LOGINNNNN!!!!");
        this.login = false;
        this.user = null;
        
        return "indexStore";
    }

    
    @PostConstruct
    public void initLoginBean ()
    {
        if(this.user == null)
        {
            login = false;
        }else
        {
            login = true;
        }
    }

    public boolean isLogin() {
        return login;
    }

    public void setLogin(boolean login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the user
     */
    public Usuario getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(Usuario user) {
        this.user = user;
    }

    public boolean isShowLogin() {
        return showLogin;
    }

    public void setShowLogin(boolean showLogin) {
        this.showLogin = showLogin;
    }
    
    
}
