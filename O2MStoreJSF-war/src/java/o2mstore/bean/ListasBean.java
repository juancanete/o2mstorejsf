/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package o2mstore.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import o2mstore.ejb.ProductoFacade;
import o2mstore.entity.Producto;

/**
 *
 * @author David
 */
@ManagedBean
@RequestScoped
public class ListasBean implements Serializable{
    @EJB
    private ProductoFacade productoFacade;
    
    private List<Producto> listProductMV;
    private List<Producto> listProductRP;
    private Producto prod;

    /**
     * Creates a new instance of ListasBean
     */
    public ListasBean() {
        
    }
    

    
    /**
     * @return the listProductMV
     */
    public List<Producto> getListProductMV() {
        return listProductMV;
    }

    /**
     * @param listProductMV the listProductMV to set
     */
    public void setListProductMV(List<Producto> listProductMV) {
        this.listProductMV = listProductMV;
    }

    /**
     * @return the listProductRP
     */
    public List<Producto> getListProductRP() {
        return listProductRP;
    }

    /**
     * @param listProductRP the listProductRP to set
     */
    public void setListProductRP(List<Producto> listProductRP) {
        this.listProductRP = listProductRP;
    }

    /**
     * @return the prod
     */
    public Producto getProd() {
        return prod;
    }

    /**
     * @param prod the prod to set
     */
    public void setProd(Producto prod) {
        this.prod = prod;
    }

    @PostConstruct
    public void encontrar(){
        listProductMV = productoFacade.findMostValued();
        listProductRP = productoFacade.findRecentPost();
        
    }

}
