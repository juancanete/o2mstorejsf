/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package o2mstore.bean;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;
import o2mstore.ejb.ProductoFacade;
import o2mstore.ejb.UsuarioFacade;
import o2mstore.entity.Producto;
import o2mstore.entity.Usuario;
import sun.misc.BASE64Encoder;

/**
 *
 * @author VictorCG
 */
@ManagedBean
@RequestScoped
public class NewProductBean {

    @EJB
    private UsuarioFacade usuarioFacade;

    @EJB
    private ProductoFacade productoFacade;

    //Bean parameters
    protected String nombre;
    protected String categoria;
    protected String descripcion;
    protected Integer usuario;
    protected Part imagen;

    protected ProfileBean profileBean;

    //Constructor
    public NewProductBean() {
        
    }
    
    //Getters and Setters
    public UsuarioFacade getUsuarioFacade() {
        return usuarioFacade;
    }

    public void setUsuarioFacade(UsuarioFacade usuarioFacade) {
        this.usuarioFacade = usuarioFacade;
    }

    public ProductoFacade getProductoFacade() {
        return productoFacade;
    }

    public void setProductoFacade(ProductoFacade productoFacade) {
        this.productoFacade = productoFacade;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getUsuario() {
        return usuario;
    }

    public void setUsuario(Integer usuario) {
        this.usuario = usuario;
    }

    public Part getImagen() {
        return imagen;
    }

    public void setImagen(Part imagen) {
        this.imagen = imagen;
    }

    public ProfileBean getProfileBean() {
        return profileBean;
    }

    public void setProfileBean(ProfileBean profileBean) {
        this.profileBean = profileBean;
    }
    
    //Own Methods
    public String createProduct() throws IOException {
        
        Producto prod = new Producto();
        prod.setIdproducto(productoFacade.getMaxID() + 1);
        prod.setNombrep(nombre);

        Usuario user = usuarioFacade.findByNum(usuario);

        prod.setPropietario(user);

        switch (categoria) {
            case "appUnEspecified":
            case "android":
            case "blackberry":
            case "iphone": {
                prod.setCategoria("aplicaciones");
                prod.setSubcategoria(categoria);
                break;
            }
            case "pelUnEspecified":
            case "terror":
            case "comedia":
            case "accion":
            case "cienciaficcion":
            case "animacion":
            case "drama":
            case "thriller": {
                prod.setCategoria("peliculas");
                prod.setSubcategoria(categoria);
                break;
            }
            case "musUnEspecified":
            case "pop":
            case "rock":
            case "metal":
            case "clasica":
            case "hiphop":
            case "disco": {
                prod.setCategoria("musica");
                prod.setSubcategoria(categoria);
                break;
            }
            case "libUnEspecified":
            case "ciencias":
            case "cocina":
            case "historia":
            case "informatica":
            case "clasicos":
            case "infantil": {
                prod.setCategoria("libros");
                prod.setSubcategoria(categoria);
                break;
            }
        }
        prod.setDescripcion(descripcion);
        prod.setFecha(new Date());
        prod.setPrecio(new BigDecimal(0));
        prod.setCalificacion(new Short("1"));
        prod.setDescargas(0);

        InputStream inputStream = imagen.getInputStream();
        FileOutputStream outputStream = new FileOutputStream(getFilename(imagen));
        long longitudimagen = imagen.getSize();

        byte[] buffer = new byte[(int) longitudimagen];
        int bytesRead = 0;
        while (true) {
            bytesRead = inputStream.read(buffer);
            if (bytesRead > 0) {
                outputStream.write(buffer, 0, bytesRead);
            } else {
                break;
            }
        }

        outputStream.close();
        inputStream.close();

        BASE64Encoder e = new BASE64Encoder();
        String datos = e.encodeBuffer(buffer);
        prod.setImagen(datos);
        
        productoFacade.create(prod);
        
        //Add to profileBean's list
        profileBean.getListProduct().add(prod);
        
        return "indexStore";
    }

    private static String getFilename(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.  
            }
        }
        return null;
    }

    //Postconstuct
    @PostConstruct
    public void encontrar() {
        FacesContext context = FacesContext.getCurrentInstance();
        LoginBean sesion = (LoginBean) context.getApplication().evaluateExpressionGet(context, "#{loginBean}", LoginBean.class);
        usuario = sesion.user.getIdusuario();
        profileBean = (ProfileBean) context.getApplication().evaluateExpressionGet(context, "#{profileBean}", ProfileBean.class);
    }
}
