/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package o2mstore.bean;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author VictorCG
 */
@ManagedBean
@RequestScoped
public class ValidationBean implements Validator{

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\." +
			"[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*" +
			"(\\.[A-Za-z]{2,})$";
 
    private Pattern pattern;
    private Matcher matcher;

    //COnstructor
    public ValidationBean(){
              pattern = Pattern.compile(EMAIL_PATTERN);
    }
    
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void validateEmail(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        matcher = pattern.matcher(value.toString());
        
        
        if((((String)value).length()==0))
        {
            throw new ValidatorException(new FacesMessage("EMAIL: Introduce un email"));
        }
        else if((((String)value).length()>50) || (((String)value).length()<2) )
        {
            throw new ValidatorException(new FacesMessage("EMAIL: Longitud del campo incorrecto"));
        }
        else if(!matcher.matches())
        {
            throw new ValidatorException(new FacesMessage("EMAIL: Direccion Email no valida"));
        }

    }

    public void validatePassword(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        
        if((((String)value).length()==0))
        {
            throw new ValidatorException(new FacesMessage("PASSWORD: Introduce una contraseña"));
        }
        else if((((String)value).length()>50) || (((String)value).length()<2) )
        {
            throw new ValidatorException(new FacesMessage("PASSWORD:  Longitud del campo incorrecto"));
        }
    }
}
