/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package o2mstore.bean;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.servlet.http.Part;
import o2mstore.ejb.UsuarioFacade;
import o2mstore.entity.Usuario;
import sun.misc.BASE64Encoder;

/**
 *
 * @author VictorCG
 */
@ManagedBean
@RequestScoped
public class SignInBean {

    @EJB
    private UsuarioFacade usuarioFacade;
    
    //Bean parameters
    private String email;
    private String password;
    private String password2;
    private String name;
    private String lastName;
    private String rol;
    private Part imagen;
    
    //Constructor
    public SignInBean() {
        
    }
    
    //Getters and Setters
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public UsuarioFacade getUsuarioFacade() {
        return usuarioFacade;
    }

    public void setUsuarioFacade(UsuarioFacade usuarioFacade) {
        this.usuarioFacade = usuarioFacade;
    }

    public Part getImagen() {
        return imagen;
    }

    public void setImagen(Part imagen) {
        this.imagen = imagen;
    }
    
    //Own Methods
    public String newUser() throws IOException
    { 
        Usuario usuario = usuarioFacade.findByEmail(email);
        
        if(usuario == null){
            
            Usuario user = new Usuario();
            user.setIdusuario(usuarioFacade.getMaxID()+1);
            user.setNombre(name);
            user.setApellidos(lastName);
            user.setContrasena(password);
            user.setEmail(email);
            user.setFechanac(new Date());
            user.setRol("user");
            
            InputStream inputStream = imagen.getInputStream();          
            FileOutputStream outputStream = new FileOutputStream(getFilename(imagen));  
            long longitudimagen= imagen.getSize();

            byte[] buffer = new byte[(int) longitudimagen];          
            int bytesRead = 0;  
            while(true) {                          
                bytesRead = inputStream.read(buffer);  
                if(bytesRead > 0) {  
                    outputStream.write(buffer, 0, bytesRead);  
                }else {  
                    break;  
                }                         
            }

            outputStream.close();  
            inputStream.close();  

            BASE64Encoder e = new BASE64Encoder();
            String datos = e.encodeBuffer(buffer);
            user.setImagen(datos);

            usuarioFacade.create(user);
            return "indexStore.xhtml";
        }
        else
        {
            return "login.xhtml";
        }
    }
    
    private static String getFilename(Part part) {  
     for (String cd : part.getHeader("content-disposition").split(";")) {  
         if (cd.trim().startsWith("filename")) {  
             String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");  
             return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.  
         }  
     }  
     return null;  
    } 
}
