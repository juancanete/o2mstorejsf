/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function servletData(category)
{
    $.ajax({
        url: "servleto2mStore?category=" + category,
        type: 'GET',
        success: function(data) {
            //parserData(urlFS);
            $("div.categories").html(data);
        },
        error: function() {
            alert('failure');
            $("div.padd p").html("Error!!!");
        }
    });
}

function buscarProd()
{
    clave1 = document.getElementById('busqueda').value;
    
    $.ajax({
        url: "busquedaServlet?busqueda="+clave1,
        type: 'get',
        success: function(data) {
            //parserData(urlFS);
            $("div.categories").html(data);
        }
    });
}

 function cargarAU(){
            $(".most-valued").hide();
            $(".box-body.recent-posts").hide();           
            $.ajax({   
                type: "GET",
                url:"/O2MStoreJSF-war/faces/xhtml/aboutUs.xhtml",
                data:{},
                success: function(data){       
                    $('#categories').html(data);
                }
            });
}

 function cargarCU(){
            $(".most-valued").hide();
            $(".box-body.recent-posts").hide();
           
            $.ajax({   
                type: "GET",
                url:"/O2MStoreJSF-war/faces/xhtml/contactUs.xhtml",
                data:{},
                success: function(data){       
                    $('#categories').html(data);
                }
            });
}

function cargarProfile(){
            $(".most-valued").hide();
            $(".box-body.recent-posts").hide();
            $.ajax({   
                type: "GET",
                url: "/O2MStoreJSF-war/faces/profile.xhtml",
                data:{},
                success: function(data){       
                    $('#categories').html(data);
                }
            });
}

function cargarSignIn()
{
    $(".most-valued").hide();
    $(".box-body.recent-posts").hide();

    $.ajax({   
        type:"GET",
        url: "/O2MStoreJSF-war/faces/signInUser.xhtml",
        data:{},
        success: function(data){
            $('#categories').html(data);
        }
    });
}

function cargarLogin(loginErr){
            $(".most-valued").hide();
            $(".box-body.recent-posts").hide();
            
            $.ajax({   
                url: "/O2MStoreJSF-war/faces/login.xhtml",
                type:"GET",
                success: function(data){
                    $('div.errMsg').html(loginErr);
                    $('#categories').html(data);
                }
            });
}

function cargarProduct(){
            $(".most-valued").hide();
            $(".box-body.recent-posts").hide();
            $.ajax({   
                type: "GET",
                url: "/o2mstore/content/html/newproduct.jsp",
                data:{},
                success: function(data){       
                    $('div.categories').html(data);
                }
            });
}



function mostrarMostValued(){
           
            $.ajax({   
                type: "GET",
                url:"/O2MStoreJSF-war/faces/xhtml/mostValued.xhtml",
                data:{},
                success: function(data){       
                    $('div.most-valued').html(data);
                }
            });
}

function mostrarRecentPost(){
           
            $.ajax({   
                type: "GET",
                url:"/O2MStoreJSF-war/faces/xhtml/recentPost.xhtml",
                data:{},
                success: function(data){       
                    $('div.box-body.recent-posts').html(data);
                }
            });
}

function comprobarContraseñas(){
    clave1 = document.f1.password.value;
    clave2 = document.f1.password2.value; 
    
    if (clave1 != clave2)
        {
            alert("Las dos claves son distintas...\nVuelve a introducir los datos") ;
            return false;
        }
        else{
            return true;
        }
        
}

function editProduct(nameProd)
{
    $(".most-valued").hide();
    $(".box-body.recent-posts").hide();
    $.ajax({
        url: "editProduct?nameprod=" + nameProd,
        type: 'GET',
        success: function(data) {
            //parserData(urlFS);
            $("div #editProduct").html(data);
        },
        error: function() {
            alert('failure');
            $("div.padd p").html("Error!!!");
        }
    });
}

function cargarEditProduct()
{
    $(".most-valued").hide();
    $(".box-body.recent-posts").hide();

    $.ajax({   
        type:"GET",
        url: "/O2MStoreJSF-war/faces/editProduct.xhtml",
        data:{},
        success: function(data){
            $('#categories').html(data);
        }
    });
}

function cargarNewProduct()
{
    $(".most-valued").hide();
    $(".box-body.recent-posts").hide();

    $.ajax({   
        type:"GET",
        url: "/O2MStoreJSF-war/faces/newProduct.xhtml",
        data:{},
        success: function(data){
            $('#categories').html(data);
        }
    });
}

function deleteProduct(idProduct)
{
    $.ajax({
        url: "deleteProduct?idProduct=" + idProduct,
        type: 'GET',
        success: function(data) {
            //parserData(urlFS);
            $("div.categories").html(data);
        },
        error: function() {
            alert('failure');
            $("div.padd p").html("Error!!!");
        }
    });
}

function closeProduct()
{
    $("div #editProduct").html('');
}

function cargarCarrito() {
    $(".most-valued").hide();
    $(".box-body.recent-posts").hide();
    $("#loadingIco").html("<img src=\"/O2MStoreJSF-war/faces/resources/images/ajax-loader.gif\" />")
    
    $.ajax({
        url: "/O2MStoreJSF-war/faces/carrito.xhtml",
        success: function(data) {
            $('#categories').html(data);
            $("#loadingIco").html("");
        }
    });
}

function buyOneProduct(idProduct)
{
    $.ajax({
        url: "buyProduct?idproduct="+ idProduct,
        type: 'POST',
        success: function() {
            alert('Producto añadido al carro.');
        },
        error: (function () {
            alert('Error al procesar el pedido.');
        })
    });
}

function finalizarPedido()
{
    $.ajax({
        url: "pedidosServlet",
        type: 'Get',
        success: function () {
            alert('Pedido realizado.');
        },
        error: (function () {
            alert('Error al realizar el pedido.');
        })
    });
}