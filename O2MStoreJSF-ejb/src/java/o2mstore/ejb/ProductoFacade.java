/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package o2mstore.ejb;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import o2mstore.entity.Producto;
import o2mstore.entity.Usuario;

/**
 *
 * @author masterinftel22
 */
@Stateless
public class ProductoFacade extends AbstractFacade<Producto> {
    
    @EJB
    private UsuarioFacade usuarioFacade;
    
    @PersistenceContext(unitName = "O2MStoreJSF-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductoFacade() {
        super(Producto.class);
    }
    
    public Producto findByName (String name)
    {
        Producto prod = (Producto) getEntityManager().createQuery("SELECT p FROM Producto p WHERE p.nombrep LIKE :nameProd")
                .setParameter("nameProd", name)
                .getSingleResult();
        
        return prod;
    }
    
    public Producto findById (Integer id)
    {
        Producto prod = (Producto) getEntityManager().createQuery("SELECT p FROM Producto p WHERE p.idproducto = :idProd")
                .setParameter("idProd", id)
                .getSingleResult();
        
        return prod;
    }
    
    public List<Producto> findByNameAll (String name)
    {
        List <Producto> list = em.createQuery("SELECT p FROM Producto p WHERE p.nombrep LIKE :nameProd")
                .setParameter("nameProd", "%"+name+"%")
                .setMaxResults(10)
                .getResultList();
        
        return list;
    }
    
    public List<Producto> findByCategory(String category)
    {
        List <Producto> list = em.createQuery("SELECT p FROM Producto p WHERE p.subcategoria LIKE :subcateg")
                .setParameter("subcateg", category)
                .setMaxResults(10)
                .getResultList();
        
        return list;
    }
    
    public List<Producto> findMostValued()
    {
        List <Producto> list = em.createQuery("SELECT p FROM Producto p ORDER BY p.calificacion DESC")
                .setMaxResults(3)
                .getResultList();
        
        return list;
    }
    
    public List<Producto> findRecentPost()
    {
        List <Producto> list = em.createQuery("SELECT p FROM Producto p ORDER BY p.fecha DESC")
                .setMaxResults(6)
                .getResultList();
        
        return list;
    }
    
    public List<Producto> findByPropietary(Integer propietario)
    {
        Usuario user = usuarioFacade.findByNum(propietario);
        
        List <Producto> list = em.createQuery("SELECT p FROM Producto p WHERE p.propietario = :propiet")
                .setParameter("propiet", user)
                .setMaxResults(10)
                .getResultList();
        
        return list;
    }
    
    public void saveProduct(Producto p){
        em.persist(p);
    }
    
    public Integer getMaxID(){
        
        Integer id = (Integer) em.createQuery("select max(p.idproducto) from Producto p order by p.idproducto desc").getSingleResult();
        
        return id;
    }
    
}
