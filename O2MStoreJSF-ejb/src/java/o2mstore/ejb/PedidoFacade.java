/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package o2mstore.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import o2mstore.entity.Pedido;

/**
 *
 * @author masterinftel22
 */
@Stateless
public class PedidoFacade extends AbstractFacade<Pedido> {
    @PersistenceContext(unitName = "O2MStoreJSF-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PedidoFacade() {
        super(Pedido.class);
    }
    
    public Integer getMaxID(){
        
        Integer id = (Integer) em.createQuery("select max(p.idpedido) "
                + "from Pedido p order by p.idpedido desc").getSingleResult();
        
        return id;
    }
    
}
