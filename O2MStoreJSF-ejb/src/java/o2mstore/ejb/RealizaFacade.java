/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package o2mstore.ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import o2mstore.entity.Realiza;

/**
 *
 * @author masterinftel22
 */
@Stateless
public class RealizaFacade extends AbstractFacade<Realiza> {
    @PersistenceContext(unitName = "O2MStoreJSF-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RealizaFacade() {
        super(Realiza.class);
    }
    
    public List<Realiza> findRealiza(Integer idusuario, Integer idpedido)
    {
              
        List <Realiza> list = em.createQuery("SELECT r FROM Realiza r WHERE r.idpedido = :pedido AND r.idusuario = :usuario")
                .setParameter("usuario", idusuario)
                .setParameter("pedido", idpedido)
                .getResultList();
        
        return list;
    }
    
    public Integer findMaxIdRealiza(){
        
        Integer id = (Integer) em.createQuery("select max(r.idrealiza) from Realiza r order by r.idrealiza desc").getSingleResult();
        
        return id;
    }
    
    public Integer findMaxIdRealizaUser(Integer idusuario){
        
        Integer id = (Integer) em.createQuery("select max(r.idrealiza) from Realiza r where r.idusuario = :value order by r.idrealiza desc")
                .setParameter("value", idusuario)
                .getSingleResult();
        
        return id;
    }
}
