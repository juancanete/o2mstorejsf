/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package o2mstore.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author masterinftel22
 */
@Entity
@Table(name = "REALIZA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Realiza.findAll", query = "SELECT r FROM Realiza r"),
    @NamedQuery(name = "Realiza.findByIdrealiza", query = "SELECT r FROM Realiza r WHERE r.idrealiza = :idrealiza")})
public class Realiza implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDREALIZA")
    private Integer idrealiza;
    @JoinColumn(name = "IDUSUARIO", referencedColumnName = "IDUSUARIO")
    @ManyToOne(optional = false)
    private Usuario idusuario;
    @JoinColumn(name = "IDPRODUCTO", referencedColumnName = "IDPRODUCTO")
    @ManyToOne(optional = false)
    private Producto idproducto;
    @JoinColumn(name = "IDPEDIDO", referencedColumnName = "IDPEDIDO")
    @ManyToOne(optional = false)
    private Pedido idpedido;

    public Realiza() {
    }

    public Realiza(Integer idrealiza) {
        this.idrealiza = idrealiza;
    }

    public Integer getIdrealiza() {
        return idrealiza;
    }

    public void setIdrealiza(Integer idrealiza) {
        this.idrealiza = idrealiza;
    }

    public Usuario getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Usuario idusuario) {
        this.idusuario = idusuario;
    }

    public Producto getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(Producto idproducto) {
        this.idproducto = idproducto;
    }

    public Pedido getIdpedido() {
        return idpedido;
    }

    public void setIdpedido(Pedido idpedido) {
        this.idpedido = idpedido;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idrealiza != null ? idrealiza.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Realiza)) {
            return false;
        }
        Realiza other = (Realiza) object;
        if ((this.idrealiza == null && other.idrealiza != null) || (this.idrealiza != null && !this.idrealiza.equals(other.idrealiza))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "o2mstore.entity.Realiza[ idrealiza=" + idrealiza + " ]";
    }
    
}
